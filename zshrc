# The following lines were added by compinstall
zstyle :compinstall filename '/Users/finger_guns/.zshrc'



# pip zsh completion start
function _pip_completion {
  local words cword
  read -Ac words
  read -cn cword
  reply=( $( COMP_WORDS="$words[*]" \
             COMP_CWORD=$(( cword-1 )) \
             PIP_AUTO_COMPLETE=1 $words[1] ) )
}
compctl -K _pip_completion pip3
# pip zsh completion end

autoload -Uz compinit
compinit
# End of lines added by compinstall
# Lines configured by zsh-newuser-install
setopt appendhistory autocd
unsetopt beep
bindkey -v
# End of lines configured by zsh-newuser-install

# Prompt stuff
autoload -Uz compinit promptinit
compinit
promptinit






[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
