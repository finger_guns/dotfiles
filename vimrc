" vimerc !!
" Basic {{
set shell=/bin/bash\ --login

" Fix backspace.
set backspace=indent,eol,start

" Modeline.
set modelines=0
set autoindent
set showmode
set showcmd
set laststatus=2

" Show me meanings of lines and all that.
set list
set listchars=tab:▸\ ,eol:¬,extends:❯,precedes:❮
set showbreak=↪

" Don't redraw unless you have to.
set lazyredraw

" Split order.
set splitbelow
set splitright

" Clipboard stuff.
set clipboard=unnamed

" Auto read write from flies and disk.
set autowrite
set autoread

" History, backup and undo stuff.
set history=1000
set undofile
set undoreload=10000

set backup
set noswapfile

set undodir=~/.tmp/undo
set backupdir=~/.tmp/backup
set directory=~/.tmp/swp

" Tabs and other text editing stuff.
set tabstop=8
set shiftwidth=4
set softtabstop=4
set shiftround
set expandtab
set wrap
set textwidth=80
set formatoptions=qrn1j
" End Basic }}

" Folding {{
set foldmethod=indent
set foldlevel=99
"" End Folding.}}

" Completion {{
set complete=.,w,b,u,t
set completeopt=longest,menuone,preview
" End Completion }}

" WildMenu {{
set wildmenu
set wildmode=list:longest
set wildignore+=.hg,.git,.svn
set wildignore+=*.aux,*.out,*.toc
set wildignore+=*.jpg,*.bmp,*.gif,*.png,*.jpeg
set wildignore+=*.o,*.obj,*.exe,*.dll,*.manifest
set wildignore+=*.spl
set wildignore+=*.sw?
set wildignore+=*.DS_Store
" End WildMenu }}


" Search {{
set ignorecase
set smartcase
set incsearch
set showmatch
set hlsearch
set gdefault
" End Search }}

" Spelling {{
set dictionary=/usr/share/dict/words
" End Spelling}}

" Colors and other appearance settings. {{
set number
set relativenumber
syntax on
set t_Co=256
set bg=dark
colors badwolf
set colorcolumn=80,120
highlight ColorColumn ctermbg=DarkGrey
" End Colors }}

" Statusline {{
function! GitBranch()
  return system("git rev-parse --abbrev-ref HEAD 2>/dev/null | tr -d '\n'")
endfunction

function! StatuslineGit()
  let l:branchname = GitBranch()
  return strlen(l:branchname) > 0?'  '.l:branchname.' ':''
endfunction

set statusline=
set statusline+=%#PmenuSel#
set statusline+=%{StatuslineGit()}
set statusline+=%#LineNr#
set statusline+=\ %f
set statusline+=%m\
set statusline+=%=
set statusline+=%#CursorColumn#
set statusline+=\ %y
set statusline+=\ %{&fileencoding?&fileencoding:&encoding}
set statusline+=\[%{&fileformat}\]
set statusline+=\ %p%%
set statusline+=\ %l:%c
set statusline+=\ 
" End Statusline }}

" Omnifunc {{
filetype plugin on
set omnifunc=syntaxcomplete#Complete
" End Omnifunc}}


" Filetypes {{
autocmd FileType python setlocal completeopt-=preview
autocmd FileType python set omnifunc=python3complete#Complete
" End Filetypes}}

" Keys {{
let mapleader = ","

" Make ; :
nnoremap : ;
nnoremap ; :

" Fix line wrap and all that
noremap j gj
noremap k gk
noremap gj j
noremap gk k

" Easy buffer moving
noremap <C-h> <C-w>h
noremap <C-j> <C-w>j
noremap <C-k> <C-w>k
noremap <C-l> <C-w>l

" Uesfull tagging things
nnoremap <leader>. :CtrlPTag<cr>

" Un highlight searches
noremap <silent> <leader><space> :noh<cr>:call clearmatches()<cr>

" Toggle nerd tree
nnoremap <Leader>f :NERDTreeToggle<Enter>
" Plugins {{
call plug#begin('~/.vim/plugged')

" Fuzzy finding.
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'

" Only motion missing from vim
Plug 'tpope/vim-surround'

" Nerdtree file browser
Plug 'scrooloose/nerdtree'

" Snippets
Plug 'SirVer/ultisnips' | Plug 'honza/vim-snippets'
if has('python3')
    " Stop deprecation warning
  silent! python3 1
endif

" Syntax checker
Plug 'w0rp/ale'

" Completionn
if has('nvim')
  Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
else
  Plug 'Shougo/deoplete.nvim'
  Plug 'roxma/nvim-yarp'
  Plug 'roxma/vim-hug-neovim-rpc'
endif
Plug 'zchee/deoplete-jedi'

Plug 'ervandew/supertab'
call plug#end()
" End Plugings }}

" Ale {{
let g:ale_linters = {
\   'python': ['flake8'],
\}
let b:ale_fixers = ['autopep8']
" End Ale }}

" FZF {{
let g:fzf_tags_command = 'ctags -R'
" END FZF }}

" Deoplete {{ 
let g:deoplete#enable_at_startup = 1
let g:deoplete#enable_ignore_case = 1
let g:deoplete#auto_complete_delay = 500
let g:deoplete#sources#jedi#python_path = '/usr/local/bin/python3'
" End Deoplete }}

" Supertab {{
let g:SuperTabDefaultCompletionType = "<c-n>"
" End Supertab }}
